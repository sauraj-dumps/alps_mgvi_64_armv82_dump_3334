#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from k6789v1_64 device
$(call inherit-product, device/alps/k6789v1_64/device.mk)

PRODUCT_DEVICE := k6789v1_64
PRODUCT_NAME := lineage_k6789v1_64
PRODUCT_BRAND := alps
PRODUCT_MODEL := k6789v1_64
PRODUCT_MANUFACTURER := alps

PRODUCT_GMS_CLIENTID_BASE := android-alps

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sys_mssi_64_ww_armv82-user 12 SP1A.210812.016 p1rck61v164bspP8 dev-keys"

BUILD_FINGERPRINT := alps/vext_k6789v1_64/k6789v1_64:12/SP1A.210812.016/p1rck61v164bspP8:user/dev-keys
